package universidade;

public class Pessoa {

  public Pessoa(){

  }

	private String nome;
	private String sobrenome;

  public void setNomeSobrenome(String nome, String sobrenome){
    this.nome=nome;
    this.sobrenome=sobrenome;
  }

	public String getEmail(){
		return this.nome+"."+this.sobrenome+"@dominio.generico.com.br";
	}

	public String getVinculo(){
		return "Sem vinculo com a UFABC";
	}

  public String getNome(){
    return this.nome;
  }

  public String getSobrenome(){
    return this.sobrenome;
  }

}
